# Ntrk Tracker Radar

A script to process privacy-focused tracking/advertising domains identified by [DuckDuckGo Tracker Radar](https://spreadprivacy.com/duckduckgo-tracker-radar/) into DNS Blocklists which can be used in NoTrack or PiHole.
  
Lists are categorised as Confirmed, High, Medium, Low, and Unknown based on the certainty of a domain being a tracker.  
Certain [categories](https://github.com/duckduckgo/tracker-radar/blob/master/docs/CATEGORIES.md) are excluded, such as: CDN, Online Payment, and Social Networking. Although domains in these categories can be used as tracking, blocking them would cause a significant noticable impact.  
Two formats of the list are provided: Hosts and Plain. Hosts can be used directly with a DNS server. Plain contains further comments such as domain ownership and purpose, it requires processing in order to be used in a DNS server. 

### Confirmed
Domains have been confirmed as Tracking, Advertising, or Malware.  
This list also contains subdomains related to tracking, e.g. metrics.cnn.com  

### High
Domains that make excessive use of browser APIs which are almost certainly for tracking purposes.

### Medium / Low
Domains that make some use of browser APIs related to tracking. These lists will likely cause some noticeable impact with legitimate domains being blocked.  

### Uncertain
Script is not yet able to determine the purpose of the site from the information provided.  
This has been kept for manual review purposes only.  
  
  
## Blocklists Host Files
Confirmed: https://gitlab.com/quidsup/ntrk-tracker-radar/-/raw/master/ddg_tracker_radar_confirmed.hosts  
High: https://gitlab.com/quidsup/ntrk-tracker-radar/-/raw/master/ddg_tracker_radar_high.hosts  
Med: https://gitlab.com/quidsup/ntrk-tracker-radar/-/raw/master/ddg_tracker_radar_med.hosts  
Low: https://gitlab.com/quidsup/ntrk-tracker-radar/-/raw/master/ddg_tracker_radar_low.hosts  
  
## Blocklists with Comments
Confirmed: https://gitlab.com/quidsup/ntrk-tracker-radar/-/raw/master/ddg_tracker_radar_confirmed.txt  
High: https://gitlab.com/quidsup/ntrk-tracker-radar/-/raw/master/ddg_tracker_radar_high.txt  
Med: https://gitlab.com/quidsup/ntrk-tracker-radar/-/raw/master/ddg_tracker_radar_med.txt  
Low: https://gitlab.com/quidsup/ntrk-tracker-radar/-/raw/master/ddg_tracker_radar_low.txt  