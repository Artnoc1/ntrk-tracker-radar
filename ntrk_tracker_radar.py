#!/usr/bin/env python3
#Title   : NoTrack Tracker Radar Processor
#Description : This script will process DuckDuckGo Tracker Radar list into five seperate files for use in a DNS server like NoTrack and PiHole.
#              Lists are categorised as Confirmed, High, Medium, Low, and Unknown based on the certainty of a domain being a tracker
#Author  : QuidsUp
#Date    : 2020-06-28
#Version : 0.2
#Usage   : git clone https://github.com/duckduckgo/tracker-radar.git
#          python3 ntrk_tracker_radar.py

#Standard Imports
import datetime
import json
import os
import re

class Ntrk_TrackerRadar():
    def __init__(self):
        #Categories are taken from [https://github.com/duckduckgo/tracker-radar/blob/master/docs/CATEGORIES.md]
        self.excluded_categories = {             #Categories to exclude which have a purpose other than tracking
            'CDN',
            'Embedded Content',
            'Federated Login',
            'Non-Tracking',
            'Online Payment',
            'Social Network',
            'SSO',
        }

        self.advertising_categories = {          #Known Advertising categories
            'Advertising',
        }

        self.tracker_categories = {              #Known Tracker categories
            'Action Pixels',
            'Ad Motivated Tracking',
            'Analytics',
            'Third-Party Analytics Marketing',
        }

        self.malware_categories = {              #Known Malware categories
            'Malware',
        }

        self.ignore_domains = {                  #Domains to exclude from complete blocking as they will cause noticeable breakage
            'amazon-adsystem.com',               #Essential for Amazon Android / iPhone app
            'azureedge.net',                     #Azure cloud services, hosts many legitimate domains
            'cloudfront.net',                    #CDN
            'cloudflare.net',                    #CDN
            'hcaptcha.com',                      #Bot Protection, used on login screens
            'walmart.com',                       #US Supermarket, also used by asda.co.uk
            'yahoo.co.jp',
        }

        self.__certain_domains = []              #Confirmed Tracker / Advertising / Malware Domains
        self.__high_domains = []
        self.__med_domains = []
        self.__low_domains = []
        self.__unknown_domains = []

        #Regex to extract domain.co.uk from subdomain.domain.co.uk
        self.__REGEX_DOMAIN = re.compile('([\w\-_]{1,63}\.(?:co\.|com\.|org\.|edu\.|gov\.)?[\w\-_]{1,63}$)')

        #Regex to identify known advertising domains by beginning subdomain
        #ads / adv, advertising, tag. Followed by optional 2-3 char domain
        self.__REGEX_ADVERTISING = re.compile('^(ad(s|v)|advertising|tag)(\.\w{2,3})?$')

        #Regex to identify known tracking subdomains
        #analytics, beacon / beacons, click, heatmap / heatmaps, insights, log / logger / logging, pixel, pixl / px / pix / pxl, stat / stats
        #telemetry, track / trck / tracker / tracking, trk (plus optional digit)
        #Optional domain .mtrcs, srvcs
        #Followed by optional 2-3 char domain
        self.__REGEX_TRACKER = re.compile('^(analytics|beacons?|click|heatmaps?|insights|log(ger|ing)?|pixel|pi?xl?|stats?|telemetry|tra?ck(er|ing)?|trk\d?)(\.mtrcs|srvcs)?(\.\w{2,3})?$')


    def __is_advertising(self, categories):
        """
        Checks if categories include Advertising categories and not Excluded categories
        """
        if categories and categories & self.advertising_categories:
            if categories and categories & self.excluded_categories:
                return False
            else:
                return True

        return False


    def __is_tracker(self, categories):
        """
        Checks if categories include Tracker categories and not Excluded categories
        """
        if categories and categories & self.tracker_categories:
            if categories and categories & self.excluded_categories:
                return False
            else:
                return True

        return False


    def __is_malware(self, categories):
        """
        Checks if categories include Tracker categories and not Excluded categories
        """
        if categories and categories & self.malware_categories:
            if categories and categories & self.excluded_categories:
                return False
            else:
                return True

        return False

    def __is_ignore(self, domain, categories):
        """
        Checks if domain is in Ignore domains
        Checks Categories does not include Excluded categories
        """
        if domain in self.ignore_domains:
            return True

        if categories and categories & self.excluded_categories:
            return True
        else:
            return False

        return False


    def __is_advertising_subdomain(self, subdomain):
        """
        Checks if subdomain matches __REGEX_ADVERTISING
        """
        if self.__REGEX_ADVERTISING.match(subdomain) is None:
            return False
        return True


    def __is_tracker_subdomain(self, subdomain):
        """
        Checks if subdomain matches __REGEX_TRACKER
        """
        if self.__REGEX_TRACKER.match(subdomain) is None:
            return False
        return True

    def __beautify_domain(self, domain):
        """
        Extract domain from subdomain
        Return UC-First domain

        Parameters:
            Domain (str)
        Returns:
            Beautified domain name
        """
        topdomain = self.__REGEX_DOMAIN.search(domain)

        if topdomain == None:                              #This shouldn't happen
            return domain.title()

        return topdomain.group(1).title()                  #TopDomain in Title case


    def __process_cnames(self, cnames, itemtype):
        """
        Process cnames
        Add all identified original domains to certain blocklist
        """
        origdomain = ''                                    #Original Domain

        if not cnames:
            return False

        for cname in cnames:
            origdomain = cname['original']

            if (self.__is_tracker_subdomain(origdomain)):  #Check if subdomain matches a tracker pattern
                self.__certain_domains.append(tuple([origdomain, self.__beautify_domain(origdomain), 'Tracker']))
            else:                                          #Otherwise use the default from DDG
                self.__certain_domains.append(tuple([origdomain, self.__beautify_domain(origdomain), itemtype]))


    def __process_subdomains(self, domain, subdomains):
        """
        Process subdomains
        Check if any subdomains match advertising or tracker regex pattern

        Parameters:
            domain (str): Domain Name
            subdomain (list): List of subdomains
        Returns:
            True: At least 1 tracker found in subdomain list
            False: Nothing useful found
        """
        containstracker = False                            #Return value to say a tracker subdomain has been found

        if len(subdomains) == 0:                           #Anything in the subdomain list?
            return False

        for subdomain in subdomains:
            if self.__is_tracker_subdomain(subdomain):
                self.__certain_domains.append(tuple([f'{subdomain}.{domain}', domain.title(), 'Tracker']))
                containstracker = True
            elif self.__is_advertising_subdomain(subdomain):
                self.__certain_domains.append(tuple([f'{subdomain}.{domain}', domain.title(), 'Advertising']))
                containstracker = True

        return containstracker


    def __process_lists(self):
        """
        Process Lists
        Sort by domain name a-z
        Display Results
        Send list to be saved to disk
        """
        self.__certain_domains.sort(key=lambda x: x[0])    #Sort by domain
        self.__high_domains.sort(key=lambda x: x[0])       #Sort by domain
        self.__med_domains.sort(key=lambda x: x[0])        #Sort by domain
        self.__low_domains.sort(key=lambda x: x[0])        #Sort by domain
        self.__unknown_domains.sort(key=lambda x: x[0])    #Sort by domain

        print('Results:')
        print(f'Confirmed Trackers: {len(self.__certain_domains)}')
        print(f'High Probability  : {len(self.__high_domains)}')
        print(f'Medium Probability: {len(self.__med_domains)}')
        print(f'Low Probability   : {len(self.__low_domains)}')
        print(f'Unknown Status    : {len(self.__unknown_domains)}')

        self.__save_list(self.__certain_domains, 'ddg_tracker_radar_confirmed.txt', 'Confirmed')
        self.__save_list(self.__high_domains, 'ddg_tracker_radar_high.txt', 'High')
        self.__save_list(self.__med_domains, 'ddg_tracker_radar_med.txt', 'Medium')
        self.__save_list(self.__low_domains, 'ddg_tracker_radar_low.txt', 'Low')
        self.__save_list(self.__unknown_domains, 'ddg_tracker_radar_unknown.txt', 'Unknown')

        self.__save_hosts(self.__certain_domains, 'ddg_tracker_radar_confirmed.hosts', 'Confirmed')
        self.__save_hosts(self.__high_domains, 'ddg_tracker_radar_high.hosts', 'High')
        self.__save_hosts(self.__med_domains, 'ddg_tracker_radar_med.hosts', 'Medium')
        self.__save_hosts(self.__low_domains, 'ddg_tracker_radar_low.hosts', 'Low')
        self.__save_hosts(self.__unknown_domains, 'ddg_tracker_radar_unknown.hosts', 'Unknown')


    def __save_hosts(self, domains, filename, listtype):
        """
        Save a blocklist as a hosts file format

        Parameters:
            domains (list): list of data to save to file
            filename (str): File to save
            listtype (str): For file comments

        Returns:
            True on success
            False on error
        """
        timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        try:
            f = open(filename, 'w')                        #Open file for ascii writing
        except IOError as e:
            print(f'Error writing to {filename}')
            print(e)
            return False
        except OSError as e:
            print(f'Error writing to {filename}')
            print(e)
            return False
        else:
            f.write(f'# DuckDuckGo Tracker Radar {listtype} Domains processed by Ntrk_TrackerRadar\n')
            f.write(f'# Original Data: https://github.com/duckduckgo/tracker-radar\n')
            f.write(f'# Project Page: https://gitlab.com/quidsup/ntrk-tracker-radar\n')
            f.write(f'# Blocklist Generated {timestamp}\n')

            for line in domains:
                f.write(f'0.0.0.0 {line[0]}\n')
            f.close()
        return True


    def __save_list(self, domains, filename, listtype):
        """
        Save a blocklist as a plainlist format

        Parameters:
            domains (list): list of data to save to file
            filename (str): File to save
            listtype (str): For file comments

        Returns:
            True on success
            False on error
        """
        timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        try:
            f = open(filename, 'w')                        #Open file for ascii writing
        except IOError as e:
            print(f'Error writing to {filename}')
            print(e)
            return False
        except OSError as e:
            print(f'Error writing to {filename}')
            print(e)
            return False
        else:
            f.write(f'# DuckDuckGo Tracker Radar {listtype} Domains processed by Ntrk_TrackerRadar\n')
            f.write(f'# Original Data: https://github.com/duckduckgo/tracker-radar\n')
            f.write(f'# Project Page: https://gitlab.com/quidsup/ntrk-tracker-radar\n')
            f.write(f'# Blocklist Generated {timestamp}\n')

            for line in domains:
                f.write(f'{line[0]} #{line[1]} - {line[2]}\n')
            f.close()
        return True


    def process_files(self, domainsdir):
        """
        Process Files

        Parameters:
            domainsdir (str): Location of DDG Tracker Radar domains folder
        """
        file_count = 0                                     #Number of files processed
        cnames = []
        domain = ''
        owner = ''                                         #Owner information or domain name

        print(f'Reading json files from {domainsdir}')
        file_list = os.scandir(domainsdir)

        for entry in file_list:
            #Ignore anything other than a .json file
            if not entry.is_file() or not entry.path.endswith('.json'):
                continue

            file_count += 1
            data = json.load(open(entry.path, "r"))        #Read JSON data from file

            domain = data['domain']
            categories = set(data['categories'])           #Convert categories list to a set
            subdomains = data['subdomains']

            if 'cnames' in data:                           #Some files lack a cname section
                cnames = data['cnames']
            else:
                cnames = []

            if not data['owner']:                          #Some files lack anything in owner
                owner = domain                             #Default to domain instead
            else:
                owner = data['owner']['displayName']

            #Process the data accordingly
            if self.__is_tracker(categories):              #1. Add known tracker categorisation to certain list
                if domain not in self.ignore_domains:
                    self.__certain_domains.append(tuple([domain, owner, 'Tracker']))
                    self.__process_cnames(cnames, 'Advertising')
                    continue

            if self.__is_malware(categories):              #2. Add known malware categorisation to certain list
                self.__certain_domains.append(tuple([domain, owner, 'Malware']))
                continue

            if self.__is_advertising(categories):          #3. Add known advertising categorisation to certain list
                if domain not in self.ignore_domains:
                    self.__certain_domains.append(tuple([domain, owner, 'Advertising']))
                    self.__process_cnames(cnames, 'Advertising')
                    continue


            if data['fingerprinting'] == 3:                #4. High certainty fingerprinting to high list
                if not self.__is_ignore(domain, categories):
                    self.__high_domains.append(tuple([domain, owner, 'Tracker High Certainty']))
                    continue

            if self.__process_subdomains(domain, subdomains): #5. Any tracking / advertising subdomains to certain list
                continue

            if data['fingerprinting'] == 2:                #6. Medium certainty to med list
                if not self.__is_ignore(domain, categories):
                    self.__med_domains.append(tuple([domain, owner, 'Tracker Medium Certainty']))
                    continue

            if data['fingerprinting'] == 1:                #7. Low certainty to low list
                if not self.__is_ignore(domain, categories):
                    self.__low_domains.append(tuple([domain, owner, 'Tracker Low Certainty']))
                    continue

            #At this point no other match has taken place.
            self.__unknown_domains.append(tuple([domain, owner, 'Unknown Status']))


        print(f'Processed {file_count} files')
        print()

        self.__process_lists()                             #Finally sort the list and then save to disk


def main():
    print('NoTrack Tracker Radar Blocklist Processor')
    print()
    trackerradar = Ntrk_TrackerRadar()
    trackerradar.process_files('../tracker-radar/domains')


if __name__ == "__main__":
    main()

